<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\VideoService;

use App\Models\Video;

class VideoController extends Controller
{
    protected $videoService;

    /**
    * @return void
    */
   public function __construct(VideoService $videoService)
   {
       $this->videoService = $videoService;
       $this->middleware('checkToken:api', ['except' => ['get']]);
   }

   /**
    * Create new Record 
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
   public function create(Request $request)
   {
        $validate = $this->validateRequest($request->all(), [
            'name' => 'required|string|between:2,100',
            'file' => 'required|string|max:100',
        ]);

        if (empty($validate)) {
            $videoData = $this->videoService->create([
                'name' => $request->name,
                'file' => $request->file
            ]);
            $responseArray = array(
                'status' => true,
                'message' => 'Video created successfully',
                'data' => $videoData
            );
            $responseCode = 200;
        }
        else {
            $responseArray = array(
                "status" => false,
                "errors" => $validate
            );
            $responseCode = 400;
        }

        return response()->json($responseArray, $responseCode);
   }

   /**
    * Update the record
    *
    * @param int $id
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
   public function edit($id, Request $request)
   {
        $validate = $this->validateRequest($request->all(), [
            'name' => 'required|string|between:2,100',
            'file' => 'required|string|max:100',
        ]);

        if (empty($validate)) {
            $videoData = $this->videoService->update([
                'name' => $request->name,
                'file' => $request->file
            ], $id);
            $responseArray = array(
                'status' => true,
                'message' => 'Video updated successfully',
                'data' => $videoData
            );
            $responseCode = 200;
        }
        else {
            $responseArray = array(
                "status" => false,
                "errors" => $validate
            );
            $responseCode = 400;
        }

        return response()->json($responseArray, $responseCode);
   }

   /**
    * Get record by ID
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function get($id)
    {
        $videoData = Video::find($id);
        return response()->json(array(
            'status' => true,
            'message' => 'Video data',
            'data' => $videoData
        ), 200);
    }

   /**
    * Validate requested values
    *
    * @param array $requestData
    * @param array $requestRules
    * @return array
    */
    private function validateRequest($requestData, $requestRules){
        $validator = Validator::make($requestData, $requestRules);
        return ($validator->fails() ? $validator->errors() : array());
    }
}
