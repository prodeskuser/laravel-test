<?php

namespace App\Repositories;

use App\Models\Video;

class VideoRepository
{
    protected $video;

    /**
    * @return void
    */
    public function __construct(Video $video)
    {
        $this->video = $video;
    }

   /**
    * Create new Record 
    *
    * @param array $dataToInsert
    */
    public function create($insertArray)
    {
        return $this->video->create($insertArray);
    }

   /**
    * Update the record
    *
    * @param array $dataToUpdate
    * @param int $id
    */
    public function update($updateArray, $id)
    {
        $videoData = Video::find($id);
        $videoData->name = $updateArray['name'];
        $videoData->file = $updateArray['file'];
        return $videoData->save();
    }
}
