<?php

namespace App\Services;

use App\Models\Video;
use App\Repositories\VideoRepository;

class VideoService
{
    protected $videoRepository;
    
    /**
    * @return void
    */
    public function __construct(Video $video)
    {
        $this->videoRepository = new VideoRepository($video);
    }

   /**
    * Create new Record 
    *
    * @param array $video
    */
    public function create($video)
    {
        return $this->videoRepository->create($video);
    }

   /**
    * Update the record
    *
    * @param array $video
    * @param int $id
    */
    public function update($video, $id)
    {
        return $this->videoRepository->update($video, $id);
    }
}
