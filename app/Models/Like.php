<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Elequent;

class Like extends Elequent
{
    use HasFactory;
    protected $connection = 'mongodb';
    protected $collection = 'likes';

    protected $fillable = [
        'video_id'
    ];
}
