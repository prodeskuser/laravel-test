<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Elequent;

class Video extends Elequent
{
    use HasFactory;
    protected $connection = 'mongodb';
    protected $collection = 'videos';

    protected $fillable = [
        'name',
        'file',
    ];

    
    public static function SCHEMAS()
    {
        return [
            'name'  => ['type' => 'string'],
            'file'  => ['type' => 'string']
        ];
    }
}
